﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>MyHome</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8-unix">
<meta name="author" content="Bart Flaherty">
<meta name="title" content="MyHome - brought to you by FosterSkills">
<meta name="description" content="Resources for foster children">
<meta name="keywords" content="">
<link rel="stylesheet" type="text/css" href="reset.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="image/x-icon" href="http://www.fosterskills.com/favicon.ico">

<?php include('/includes/head.html'); ?>

<style type="text/css">

.bg{
        min-height: 100%;
        min-width: 1024px;

        width: 100%;
        height: auto;
        
        position: fixed;
        top: 0;
        left: 0;
}

div.wrapper{
position:relative;
width:1024px;
min-height:835px;
height:auto;
margin: 0 auto;
}

div.left{
float:left;
width:135px;
margin-left:15px;
margin-top:15px;
height:100%;
background-color:lightsalmon;
}

div.leftnav{
float:left;
width:135px;
height:120px;
background-color:mediumaquamarine;
}

div.topleft{
margin-left:35px;
width:400px;
height:250px;
background-color:aliceblue;
}

div.topright{
width:424px;
height:250px;
margin-left:15px;
background-color:beige;
}

div.main{
float:left;
width:490px;
min-height:300px;
height:auto;
margin-left:35px;
margin-right:35px;
background-color:burlywood;
}

</style>

</head>

<body>

<?php include('/includes/prewrapper.html'); ?>
<div><img class="bg" src="/bg.jpg" alt="background"></div>
<div class="wrapper">

<div class="left">
<div class="leftnav"><p>yo</p></div> 
<div class="leftnav"><p>yo</p></div> 
<div class="leftnav"><p>yo</p></div>
<div class="leftnav"><p>yo</p></div>
<div class="leftnav"><p>yo</p></div>
<div class="leftnav"><p>yo</p></div>
<div class="leftnav"><p>yo</p></div>
</div> <!-- END LEFT -->

<div class="topleft"> 
<p>yo</p>
</div> <!-- END TOPLEFT -->

<div class="topright">
<p>yo</p>
</div> <!-- END TOPRIGHT -->

<div class="main"> <!-- 490 width 30px left margin height auto -->
<div class="header">
<p>yo</p>
</div> <!-- END HEADER -->
<div class="content">
<p>yo</p>
</div> <!-- END CONTENT -->
</div> <!-- END MAIN -->

<div class="right">
<div class="search">
<p>yo</p>
</div> <!-- END SEARCH -->
<div class="tellus">
<p>yo</p>
</div> <!-- END TELLUS -->
<div class="announcements">
<p>yo</p>
</div> <!-- END ANNOUNCEMENTS -->
</div> <!-- END RIGHT -->


</div> <!-- END WRAPPER -->
</body>
</html>
































<!--TOP-->
<div class="top">
<p class="quote">
"We cannot always&nbsp&nbsp<br>
build the future for our youth, but&nbsp&nbsp<br>
we can build our youth for the future."
</p>
<p class="source">Franklin D. Roosevelt</p>
<a href="http://www.fosterskills.org/home.html"><img class="logo" src="http://www.fosterskills.org/images/Foster_Skills_Inc.png" alt="Foster Skills logo"> </a>

<div class="navbar"><p class="navbar-text"><a href="http://www.fosterskills.org/aboutus.html" class="navbar-text">about us</a></p></div>
<div class="navbar"><p class="navbar-text"><a href="http://www.fosterskills.org/voices.html" class="navbar-text">voices</a></p></div>
<div class="navbar"><p class="navbar-text"><a href="http://www.fosterskills.org/get_involved.html" class="navbar-text">get involved</a></p></div>
<div class="navbar"><p class="navbar-text"><a href="http://www.fosterskills.org/resources.html" class="navbar-text">resources</a></p></div>
<div class="navbar"><p class="navbar-text"><a href="http://www.fosterskills.org/contact_us.html" class="navbar-text">contact us</a></p></div>
</div> <!-- End TOP div -->

<!-- MAIN -->
<div class="main" style="background-image:url(http://www.fosterskills.org/images/home_main-image.jpg);">
<div class="home-text-div">
  <h1 class="home-page-text">Foster Skills is a nonprofit organization dedicated to helping foster youth beat the odds and achieve life success.</h1>

  <p class="home-page-text">
Founded in 2010, we are the only nonprofit in Massachusetts<br>
that exclusively focuses on equipping foster youth with<br>
practical life skills for success in the world today.<br></p>  
</div>
</div> <!-- End MAIN div -->

<!-- BOX 1 -->
<div class="lower-box-1">
<a href="http://www.fosterskills.org/get_involved/donate.html"><img class="outer-image" src="http://www.fosterskills.org/images/lower_box_donate.png" alt="donate to foster skills"></a>
<p class="outer-text-header"><a href="http://www.fosterskills.org/get_involved/donate.html" class="outer-text-header">SUPPORT US</a></p><p class="outer-text">

We have energy and<br>
passion, but we need<br>
support to change the lives<br>
of foster youth.<br><br>
<b><strong style="font-weight:bold"><a href="http://www.fosterskills.org/get_involved/donate.html" class="outer-text">Your donation works to</a><br>
build a brighter future<br>
for these children.</b></strong></p>
</div> <!-- END BOX 1 -->

<!-- BOX 2 -->
<div class="lower-box-2">
<img class="outer-image" src="http://www.fosterskills.org/images/lower_box_gradcap.png" alt="graduation cap">
<p class="outer-text-header">DID YOU KNOW?</p><br>
<p class="outer-text">
Foster children face<br>
very challenging odds:<br><br><br>
<strong style="font-weight:bold">LESS THAN 3%</strong><br>
of former foster youth obtain a bachelor's<br>
degree. <a href="http://www.fosterskills.org/voices.html" class="outer-text">Read some success stories here.</a>

</div> <!-- END BOX 2 -->

<!-- BOX 3 -->
<div class="lower-box-3"> 
<p class="outer-text-header">SPOTLIGHT</p>
<p class="outer-text">
We're thrilled that <a href="http://www.twr08.org" class="outer-text">Together We Rise</a> has<br>
joined forces with Foster Skills on a national campaign: <b>'Together We Foster Skills'</b>.<br><br>

<a href="http://www.fosterskills.org/mou.pdf">
<img class="outer-image" src="http://www.fosterskills.org/images/together_we_rise.png"  alt="Together We Rise"></a>
<a href="http://www.fosterskills.org/mou.pdf" class="outer-text">
<br>Click here to learn<br>
more about this<br>
exciting partnership!</a></p>
</div> <!-- END BOX 3 -->

<!-- FOOTER -->
<div class="footer">
<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-html401" alt="Valid HTML 4.01 Strict" height="20" width="58" style="float:right; margin-top:8px;"></a>
<p class="footer-text">Copyright 2011 Foster Skills, Inc. All rights reserved<br> Website by Nicole Fichera and Bart Flaherty</p>

</div>
</div> <!-- ends the wrapper div -->

</body>
</html>

